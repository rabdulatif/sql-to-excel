﻿namespace SqlResultExcel.Logics
{
    /// <summary>
    /// 
    /// </summary>
    public class ExportSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int RecordPerSheet { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SavedPath { get; set; }
    }
}
