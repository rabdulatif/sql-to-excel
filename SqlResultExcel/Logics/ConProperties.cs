﻿namespace SqlResultExcel.Logics
{
    /// <summary>
    /// 
    /// </summary>
    public class ConProperties
    {
        public string ConnectionString { get; set; }
        public string FolderPath { get; set; }
        public int RecordPerSheet { get; set; }
        public string ExcelFileName { get; set; }
    }
}
