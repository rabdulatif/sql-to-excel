﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using OfficeOpenXml;

namespace SqlResultExcel.Logics
{
    /// <summary>
    /// 
    /// </summary>
    public class ExportHelper
    {
        public const string DateFormat = "yyyyMMdd_HH_mm";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="settings"></param>
        public static void ExportToExcel(DataTable dataTable, ExportSettings settings)
        {
            var totalRows = dataTable.Rows.Count;
            var sheetCount = (int)Math.Ceiling((double)totalRows / settings.RecordPerSheet);
            var excelPackage = new ExcelPackage();

            for (var sheet = 0; sheet < sheetCount; sheet++)
            {
                var worksheet = excelPackage.Workbook.Worksheets.Add($"Sheet{sheet}");
                var topData = dataTable.AsEnumerable()
                    .Skip(sheet * settings.RecordPerSheet)
                    .Take(settings.RecordPerSheet)
                    .CopyToDataTable();

                worksheet.Cells.LoadFromDataTable(topData, true);
            }

            SaveFileAs(excelPackage,out var savedPath);
            settings.SavedPath = savedPath;
            excelPackage.Dispose();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excelPackage"></param>
        /// <param name="savedPath"></param>
        private static void SaveFileAs(ExcelPackage excelPackage,out string savedPath)
        {
            SaveFileDialog saveFile=new SaveFileDialog();
            saveFile.Filter = @"Excel files(*.xlsx)|*.xlsx";
            saveFile.FileName = DateTime.Now.ToString(DateFormat);

            if (saveFile.ShowDialog() == DialogResult.Cancel)
            {
                savedPath = string.Empty;
                return;
            }

            var fileName = saveFile.FileName;
            excelPackage.SaveAs(new FileInfo(fileName));
            savedPath = fileName;
        }
    }
}