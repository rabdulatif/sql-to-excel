﻿using System.Data;
using System.Data.SqlClient;

namespace SqlResultExcel.Logics
{
    /// <summary>
    /// 
    /// </summary>
    public class QueryHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static DataTable ExecuteSqlQuery(string query, SqlConnection connection)
        {
            var dataTable = new DataTable();
            using (var adapter = new SqlDataAdapter(query, connection))
                adapter.Fill(dataTable);

            return dataTable;
        }
    }
}