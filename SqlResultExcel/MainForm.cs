﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;
using DevExpress.XtraEditors;
using SqlResultExcel.Logics;
using ExportHelper = SqlResultExcel.Logics.ExportHelper;

namespace SqlResultExcel
{
    /// <summary>
    /// 
    /// </summary>
    public partial class MainForm : XtraForm
    {
        /// <summary>
        /// 
        /// </summary>
        public readonly ExportSettings Settings;

        /// <summary>
        /// 
        /// </summary>
        public SqlConnection Connection;

        /// <summary>
        /// 
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            Settings = new ExportSettings();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                TrySwitchLayoutAndInitializeProperties();
                await TrySwitchConnection();
                btnConnect.Enabled = true;
                layoutControlGroup2.Enabled = true;
            }
            catch (Exception exception)
            {
                OnConnectDatabaseError(exception);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void TrySwitchLayoutAndInitializeProperties()
        {
            btnConnect.Enabled = false;
            if (layoutControlGroup3.Enabled)
            {
                InternalInitializeProperties();
                layoutControlGroup3.Enabled = false;
                layoutControlGroup2.Enabled = false;
                btnConnect.Text = @"Disconnect";
            }
            else
            {
                layoutControlGroup3.Enabled = true;
                layoutControlGroup2.Enabled = true;
                btnConnect.Text = @"Connect";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void InternalInitializeProperties()
        {
            if (string.IsNullOrEmpty(txtServer.Text) || string.IsNullOrEmpty(txtDatabase.Text))
                throw new Exception("Сервер и база данных обязательны для заполнения!");
            if (string.IsNullOrEmpty(txtLogin.Text) || string.IsNullOrEmpty(txtPassword.Text))
                Settings.ConnectionString = $"Data Source={txtServer.Text};Initial Catalog={txtDatabase.Text};Integrated Security=true;";
            else
                Settings.ConnectionString = $"Data Source={txtServer.Text};Initial Catalog={txtDatabase.Text};User id={txtLogin.Text};Password={txtPassword.Text};";

            Settings.RecordPerSheet = (int)numbRecordsPerSheet.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        private async Task TrySwitchConnection()
        {
            Connection = new SqlConnection(Settings.ConnectionString);

            if (layoutControlGroup3.Enabled)
            {
                Connection?.Close();
                Connection.Dispose();
            }
            else
            {
                await Connection.OpenAsync();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExecute_Click(object sender, EventArgs e)
        {
            try
            {
                TryExportToExcel();
            }
            catch (Exception ex)
            {
                OnExportToExcelError(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void TryExportToExcel()
        {
            btnExecute.Enabled = false;
            var datatable = QueryHelper.ExecuteSqlQuery(txtSqlQuery.Text, Connection);

            ExportHelper.ExportToExcel(datatable, Settings);
            if (!string.IsNullOrEmpty(Settings.SavedPath))
                Process.Start(Settings.SavedPath);
            btnExecute.Enabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        private void OnConnectDatabaseError(Exception ex)
        {
            btnConnect.Text = @"Connect";
            layoutControlGroup3.Enabled = true;
            btnConnect.Enabled = true;
            XtraMessageBox.Show($"{ex.Message}, {ex.InnerException?.Message}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        private void OnExportToExcelError(Exception ex)
        {
            btnExecute.Enabled = true;
            XtraMessageBox.Show($"{ex.Message}, {ex.InnerException?.Message}");
        }

    }
}